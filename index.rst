.. SFforEngineer documentation master file, created by
   sphinx-quickstart on Mon Mar 31 11:00:47 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

エンジニアのためのSF作品
=========================================

Contents: 目次

| ここではエンジニアが知っておくべきSF作品を蓄積していくことを目的としています。
| 小説、アニメ、映画、TV番組、ゲームまでジャンルは何でもOKです。
| エンジニアとしてのインスピレーションを高められると思う作品はガンガン追加していって下さい。
|
| リポジトリはこちらになります。 https://bitbucket.org/braitom/sfforengineer/

.. toctree::
   :maxdepth: 2

   movies.rst
   tvshows.rst
   novels.rst
   animes.rst
   cartoons.rst
   games.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

