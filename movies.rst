##################
映画
##################

2001年宇宙の旅
===========================

内容
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| キューブリック（監督）＆クラーク（SF作家）が60年代に制作したSF映画の金字塔。
| 当時最先端の科学知見を元にした、徹底的リアル志向の映像。アポロの月着陸前に制作されたとはとても思えないクォリティ。
| 反面、冗長な説明を極限まで切り詰めた演出のせいで、予備知識なしではストーリーをほとんど理解できない。というか、普通の人の感想は大抵「長くてダルいわけわかんない映画」という感じになると思われる。
| 解説サイトを参照するか、詳しい人にリアルタイムで音声解説してもらいながら見るのがおすすめ。
| `2001年宇宙の旅解説`_

見どころ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

HAL9000コンピュータ
  | 木星探査宇宙船ディスカバリー号の管理を行う、人工知能を備えたコンピュータ。通称HAL（ハル）。
  | 宇宙船の乗組員と自然な会話が行えるのはもちろん、主人公の描いたスケッチを見て「うまくなりましたね」と答えたり、自分が機能停止される（≒殺される）ことを恐れるなど、人間と同様の感情を持つ存在として描かれている。ただしHALの発声は常に抑制がきいており、声のニュアンスから感情の変化をうかがうことはできない。
  | 船内の至るところに、消火栓のランプのような形のHALの端末が設置されている。HALはこの端末を通じて乗組員と会話したり、端末内のカメラで船内の様子を確認したりする。
  | HALの中枢は船内の「ロジックメモリーセンター」という部屋に格納された大量のメモリーユニット群。主人公がユニットを引き抜くたびにHALの知能が低下していくシーンは衝撃的。
  | https://www.youtube.com/watch?v=c8N72t7aScY

テレビ電話
  | 地球周回軌道上のステーション⇔地球の一般家庭間で行われるリアルタイムのビデオ通話システム。
  | 電話ボックス風のブース内に通話端末が設置されており、公衆電話の要領でカードを挿入してからダイヤルボタンを押して相手を呼び出す。終話後は画面に通話料金が表示される。通話相手が表示されるモニタは正方形に近い比率。
  | 一般家庭側端末の形態は不明だが、人物は手ぶらで屋内を動き回りながら会話している。音声やジェスチャによる着信応答システムや、通話中の人物の顔をトレースするカメラが家庭内に設置されていると思われる。
  | https://www.youtube.com/watch?v=vWwo6JpMceg

声紋認証システム
  | 地球周回軌道上のステーションへの入管手続きで使われる。
  | 端末上の（物理的な）ボタンで（自然）言語を選ぶと、選んだ言語による音声アナウンスが始まる。これに従って自分の名前、行先等を発声すると認証され、ゲートが開く。

タブレット端末
  | 推定10-15インチ程度のモニタといくつかのボタンらしきものを備えた携帯端末。主にポートレート（縦長）モードで使う様子。
  | 劇中では、食事中に卓上に置いてテレビを見るくらいにしか使われていない。この世界のテレビ番組はポートレート比率での配信にも対応しているようである。
  | http://www.tvforum.co.uk/tvhome/fictonal-presentation-33256/

演出全般
  - 無音の宇宙空間を表現するため、船外活動のシーンは基本的に無音、または登場人物の呼吸音が聞こえるだけ。爆発音とかが一切ない。
  - 劇中に登場するモニタ内の表示、説明書き、ロゴマーク等には欧文の定番書体が使われており、これも本作が古びない理由の一つと思われる。よく使われているのは、Futura, Univers, Eurostile（Bold Extended）等のサンセリフ書体。
  | http://typesetinthefuture.com/2001-a-space-odyssey/


Star Warsシリーズ
===========================

内容
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| 言わずと知れたSF業界の名作。
| エピソード4~6が昔公開され、エピソード1~3が比較的最近公開された。これを見ずにSFは語れない。
| 宇宙船とか、武器とかかっちょいい。フォースを感じて見るもの。
| `スター・ウォーズ・シリーズ(Wikipedia)`_

ブレードランナー
===========================

内容
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| 「アンドロイドは電気羊の夢を見るか？」が原作。アンドロイドとは何か？について非常に考えさせられる作品。
| 「通常版」、「ディレクターズ・カット版」、「ファイナルカット版」と存在してそれぞれ微妙に内容が違う。
| `ブレードランナー(Wikipedia)`_

見どころ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
レプリカント
  | 遺伝子工学によって作られた人造人間。
  | レプリカントと人間は外見上まったく区別が付かないため、「フォークトカンプフ検査」という心理テスト風検査を受けさせ、その結果から識別するしかない。
  | 人間の嫌う危険・過酷な環境での奴隷労働に従事させられることが多い。超人的な身体能力を有する反面、極端に短命。
  | 劇中では、タイレル社の最新型レプリカントである「ネクサス6型」数名が宇宙植民地から逃走して地球に潜伏。これを追跡・処刑するのがハリソン・フォード扮する「ブレードランナー」の役割である。

情報解析機「ESPER」
  | プリントされた写真を端末にスキャンさせてモニタに表示→音声コマンドで拡大縮小、表示範囲移動、印刷を指示。
  | 表示範囲移動は、中心座標らしき数値を声で指定する。よく数値を空で言えるもんである。
  | 拡縮／移動時は、画面全体がフラッシュを繰り返しながら徐々に視点変化していく。
  | 画像は通常ではありえないレベルで超拡大し、隅々のディテールまで確認可能。撮影位置を後処理でずらすと後ろに隠れていた部分が見える…といった描写もある。
  | 複数視点を超高解像度で撮影できるカメラで撮影され、そのデータがプリント内に埋め込まれているのかもしれない。
  | https://www.youtube.com/watch?v=qHepKd38pr0
  | http://amelia066.blog62.fc2.com/blog-entry-2211.html

演出全般
  - スピナー（垂直離着陸・飛行可能な乗用車）
  - 宇宙植民地への移民を叫ぶ巨大広告飛行船
  - ビルの壁面全体を覆う「強力わかもと」のビデオ広告、妖艶な笑みを浮かべる芸者の映像
  - 人工眼球の培養を行う業者
  - 降りしきる酸性雨、柄が発光する傘
  - 日本語の断片が使われたネオン看板による、アジア的な猥雑な雰囲気
  - 「2つで十分ですよ」
  | …などから構成される退廃的な未来世界のビジュアルイメージは強烈で、多くのパクリを生み出した。


エイリアンシリーズ
===========================

内容
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| エイリアン1~4まである。
| 特に名作はエイリアン２。この時代にこれだけ未来を描けているのはすごい。最後に主人公はパワードスーツでエイリアンと戦うのは見もの。
| `エイリアン(Amazon検索結果)`_

見どころ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
コンピュータとのコミュニケーション（エイリアン1）
  | 宇宙船の管理コンピュータ「マザー」のコンソールはコマンドライン風UI。キーボード操作でメニュー画面からコマンドを選択する描写がある。
  | コマンド選択後は自然言語で対話できる。ただし一問一答形式のテキストチャット風インタラクションである。
  | 人間と見分けのつかないアンドロイドが登場する本作において、コンピュータとの対話だけがなぜこんなにも原始的？と思ってしまうが、おそらく「冷徹なコンピュータ」を印象づけるための演出だろう。
  | テキスト表示のエフェクトはめちゃくちゃかっこいい（下記動画の1分くらいから）。
  | https://www.youtube.com/watch?v=2ywWFvjE-yU

宇宙船自爆装置（エイリアン1）
  | 自爆専用のコンソールに用意されたシリンダーに4本の起爆用プラグを挿入すると、一定時間後に自爆が実行される。
  | 自爆装置起動後は船内アナウンスで「自爆キャンセル可能な時間」と「自爆までの時間」が定期的に通知される。
  | 自爆をキャンセルするには、「自爆キャンセル可能な時間」に達するまでにすべてのカートリッジをシリンダーから抜かなければならない。
  | シリンダーはスクリューマウントで、プラグの着脱にはネジ込み操作が必要。この作業は明らかに効率が悪そうで（自爆が必要なほどの）緊急事態に適切なUIとは言えない。
  | ちなみに、本作の自爆に関する一連の演出は、押井守監督がアニメ映画「機動警察パトレイバー the Movie」で盛大にパクっている。
  | http://www.zen171398.zen.co.uk/Alien%20Page%206/Ian%20Wingrove-auto%20destruct02.jpg

パワーローダー（エイリアン2）
  | 作業用の人型パワードスーツ。
  | 全高3mくらいあり、重機のようなイメージ。腕の先はカニ爪。
  | 搭乗者の身体動作をトレースするように機械の四肢が動く、いわゆる「マスタースレーブ」UI。
  | https://www.youtube.com/watch?v=FSrcMaid0mg

モーショントラッカー（エイリアン1,2）
  | エイリアンの探知に使われるセンサーで「空気の流れ」を探知する。XY平面上の動体の位置をモニタに表示＆動体との距離をアラーム音で知らせる。
  | 高さを知る術がないので、3次元的に込み入った構造の宇宙船内ではほとんど使い物になっていない。
  | http://www.therpf.com/members/zorger/albums/zorger-s-collection/164638-aliens-motion-tracker-330-2500-master-replicas.jpg

セントリーガン（エイリアン2）
  | 遠隔操作式の重機関銃。動体を感知して自動的に発砲する。
  | 使用者は専用のポータブルコンソールのモニタで動作状況や残弾数をリアルタイムで確認できる。
  | 限られた弾数がじりじりと減っていく緊張感をモニタのクローズアップで表現する演出はめちゃくちゃかっこよく、以後無数の映像作品で模倣されている（たとえば「新世紀エヴァンゲリオン」とか）。
  | https://www.youtube.com/watch?v=HQDy-5IQvuU

演出全般
  - モニタを覗き込む搭乗人物の顔に画面の内容が映り込む演出（エイリアン1,2）。これは「2001年宇宙の旅」が初出かも。
  - 生命反応をスキャンするレーザー光（エイリアン2）。これも「機動警察パトレイバー the Movie」でパクられている。
  - コールドスリープ装置や医療機器のプロダクトデザイン。丸みを持った清潔感のある白色で統一されており、最新の医療用機器のデザインコンセプトにも通じる。


プレデターシリーズ
===========================

内容
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| プレデター1~2まである。とりわけプレデター1がアツい。シュワちゃんとプレデターの一騎打ちは激アツ。
| 私の中ではプレデターこそが戦闘用のウェアラブル機器を身につけた究極の姿だと考えている。
| `プレデター(Wikipedia)`_
| `プレデター2(Wikipedia)`_

見どころ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
コンピューターガントレット
  | 腕に装着したコンピュータで、光学迷彩、ヘルメットの表示モード、個人用の自爆装置等をコントロールできる。
  | 小さなモニタとキーボードが付いており、キーで操作しているように見える。
  | かっこいいけど、戦闘中に画面を見ながらキー操作する＝敵から目を離す というのは致命的な気がするのだが…
  | http://ja.wikipedia.org/wiki/%E3%83%97%E3%83%AC%E3%83%87%E3%82%BF%E3%83%BC_(%E6%9E%B6%E7%A9%BA%E3%81%AE%E7%94%9F%E7%89%A9)#.E8.A3.85.E5.82.99

プレデターズ
===========================

内容
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| 今までのプレデターとはちょっと毛色が違うが面白い。2010年公開と比較的新しい。
| 個人的には日本刀での一騎打ちは激アツ
| `プレデターズ(Wikipedia)`_

.. _2001年宇宙の旅解説: http://www.geocities.co.jp/Hollywood-Theater/9529/cinema/2001s.html
.. _スター・ウォーズ・シリーズ(Wikipedia): http://ja.wikipedia.org/wiki/%E3%82%B9%E3%82%BF%E3%83%BC%E3%83%BB%E3%82%A6%E3%82%A9%E3%83%BC%E3%82%BA%E3%83%BB%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA
.. _ブレードランナー(Wikipedia): http://ja.wikipedia.org/wiki/%E3%83%96%E3%83%AC%E3%83%BC%E3%83%89%E3%83%A9%E3%83%B3%E3%83%8A%E3%83%BC
.. _エイリアン(Amazon検索結果): http://www.amazon.co.jp/s/ref=nb_sb_noss_1?__mk_ja_JP=%E3%82%AB%E3%82%BF%E3%82%AB%E3%83%8A&url=search-alias%3Ddvd&field-keywords=%E3%82%A8%E3%82%A4%E3%83%AA%E3%82%A2%E3%83%B3
.. _プレデター(Wikipedia): http://ja.wikipedia.org/wiki/%E3%83%97%E3%83%AC%E3%83%87%E3%82%BF%E3%83%BC_(%E6%98%A0%E7%94%BB)
.. _プレデター2(Wikipedia): http://ja.wikipedia.org/wiki/%E3%83%97%E3%83%AC%E3%83%87%E3%82%BF%E3%83%BC2
.. _プレデターズ(Wikipedia): http://ja.wikipedia.org/wiki/%E3%83%97%E3%83%AC%E3%83%87%E3%82%BF%E3%83%BC%E3%82%BA